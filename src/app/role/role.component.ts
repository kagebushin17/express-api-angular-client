import { Component, OnInit } from '@angular/core';
import { RoleService } from './shared/role.service';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css'],
  providers: [RoleService]
})
export class RoleComponent implements OnInit {

  constructor(private roleService: RoleService) { }

  ngOnInit() {
  }

}
