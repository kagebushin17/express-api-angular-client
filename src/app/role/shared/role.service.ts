import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
//import { Observable } from 'rxjs';
//import 'rxjs/add/operator/toPromise';


import { Role } from '../shared/role.model';
@Injectable({
  providedIn: 'root'
})
export class RoleService {

  selectedRole : Role;
  roleList : Role[];
  uri : string = 'http://localhost:3000/roles';


  constructor(private http : Http) { }

  getRoleList() {
    this.http.get(this.uri).subscribe(
      response => {
        this.roleList = response.json();
      }
    ); 
  }
  
}

