import { RoleService } from './../shared/role.service';
import { Component, OnInit } from '@angular/core';
//import { Role } from '../shared/role.model';


@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {

  constructor(private roleService: RoleService) { }

  ngOnInit() {
    this.roleService.getRoleList();
  }

}
